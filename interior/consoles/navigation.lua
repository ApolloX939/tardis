
minetest.register_node("tardis:nav_console", {
    short_description = "Navigation Console",
    description = "Navigation Console\nUsed to set your destination",
    tiles = {
        "x_console_1.png",
        "x_console_1.png",
        "tardis_side_1.png",
        "tardis_side_1.png",
    },
    groups = {cracky = 3},
    after_place_node = function (pos, placer)
        local name = placer:get_player_name()
        local user = _tardis.get_user(name)
        if user.version == nil then
            minetest.chat_send_player(name, "You must have a Tardis first")
        end
        local pos1 = vector.new(user.out_pos)
        pos1:add()
        local pos2 = vector.new(user.out_pos)
    end
})
